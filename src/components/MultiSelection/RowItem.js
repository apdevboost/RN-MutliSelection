import React, {Component} from 'react';
import {Text, TouchableOpacity, View, TextInput, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {moderateScale,} from '../../utils/scaling';
import Search from '../Search'

class RowItem extends Component {

  static propTypes = {
    item: PropTypes.object,
    onPress: PropTypes.func
  };

  constructor(props: PropsType) {
    super(props);
  }

  render() {
    return (
        <View> 
        <TouchableOpacity 
        activeOpacity={1}
        style={{
            flexDirection: 'row',
            padding: 10,
            borderBottomWidth: 1,
            borderStyle: 'solid',
            borderColor: '#ecf0f1'}}
            onPress={() => {this.props.onPress(this.props.item)}}>
            {this.props.item.isSelected? 
            <Icon name="checkbox-marked" size={30} color={'#777777'}></Icon> 
            : <Icon name="checkbox-blank-outline" size={30} color={'#D3D3D3'}></Icon>}
              {/* <View style={{
                opacity: 0.5,}}>
                {isSelected?
                  (<Icon name="checkbox-marked" size={30} color={'#777777'}></Icon>)
                  :(<Icon name="checkbox-blank-outline" size={30} color={'#777777'}></Icon>)}
              </View> */}

              <View style={{
                flex: 1,
                marginLeft: 16,
                alignItems: 'flex-start',
                justifyContent: 'center',
                }}>
                {this.props.item.isSelected? 
                  (<Text style={{fontWeight: 'bold', color: '#777777'}}>{`${this.props.item.name}`}</Text>)
                  :(<Text style={{color: '#D3D3D3' }}>{`${this.props.item.name}`}</Text>)}
              </View>
            </TouchableOpacity>
    </View>
   
    );
  }
}

const styles = StyleSheet.create({
    multiSelectionContainer: {
    flex: 1,
    alignItems: 'stretch',
  },

});

export default RowItem;