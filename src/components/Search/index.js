import React, {Component} from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {moderateScale} from '../../utils/scaling';

class Search extends Component {

  static propTypes = {
    onChangeText: PropTypes.func
  };

  render() {
    return (
        <View style={styles.InputTextContainer}>
            <TextInput
                style={styles.searchInput}
                underlineColorAndroid='transparent'
                placeholder="Search..."
                onChangeText={(text) => this.props.onChangeText(text)}/>
            <Icon
                name="magnify"
                size={30}
                color={'#000000'}
                style={{flex: 1 , 
                position: 'absolute', 
                right: '4%', 
                top: '16%', 
                alignItems: 'center',
                justifyContent: 'center' }}/>    
        </View>
    );
  }
}

const styles = StyleSheet.create({
  searchContainer: {
    backgroundColor: '#FF0000'
  },
  InputTextContainer: {
    marginTop: moderateScale(16),
    marginBottom: 0,
    flexDirection: 'row',
    borderColor: '#ddd',
    backgroundColor: '#f8f8f8',
    borderWidth: moderateScale(1),
    height: 47,
    justifyContent: 'space-between',
    alignSelf: 'stretch',
  },
  searchInput: {
    paddingLeft: 10,
    paddingRight: '12%',
    paddingVertical: 5,
    backgroundColor: '#fff',
    width: '100%',
    fontSize: 16,
  },
});

export default Search;